import map from '../../src/map.js';

 function square(n) {
       return n * n;
     }

 function tenPercentDiscount(n) {
        return Number((n - (n/10)).toFixed(2));
      }

test('Toteuttaa arrayn [4,8] jokaisen arvon funktiolla square', () => {
    expect(map([4,8], square)).toStrictEqual([16, 64]);
});

test('Toteuttaa arrayn [4.30, 50.00] jokaisen arvon funktiolla tenPercentDiscount', () => {
    expect(map([4.30, 50.00], tenPercentDiscount)).toStrictEqual([3.87, 45.00]);
});