import ceil from '../../src/ceil.js';

test('Positiivisen luvun pyöristäminen ylös antaa yhdellä parametrilla kokonaisluvun', () => {
    expect(ceil(3.001)).toBeCloseTo(4, 0);
});

test('Positiivisen luvun pyöristäminen ylös toisen desimaalin tarkkuuteen', () => {
    expect(ceil(3.001,2)).toBeCloseTo(3.01, 2);
});

test('Negatiivisen luvun pyöristäminen', () => {
    expect(ceil(-45.11)).toBeCloseTo(-45.0, 0);
});

test('Luvun pyöristäminen kahden ensimmäisen luvun tarkkuuteen', () => {
    expect(ceil(1234.567, -2)).toBeCloseTo(1300,0);
});
