import memoize from '../../src/memoize.js';
// This test is not functional
const object = { 'a': 1, 'b': 2 };
const other = { 'c': 3, 'd': 4 };

function values(object){
	var a = [];
	for(var val in object){
		a.push(object[val]);
	}
	return a;
}

const memValues = memoize(values);

test('memoize object{ a: 1, b: 2 } and remember it´s values even object.a value has changed', () => {
	expect(memValues(object)).toEqual([1, 2]);
    object.a = 2;
    expect(memValues(object)).toEqual([1, 2]);
});


test('memoize object{ c: 3, d: 4 } and remember it´s values other.d value has changed', () => {
	expect(memValues(other)).toEqual([3, 4]);
    other.c = 1;
    expect(memValues(other)).toEqual([3, 4]);
});

test('memoize object { a: a, b: b } and  change  catche values to [a, b]', () => {
	memValues.cache.set(object, ['a', 'b']);
	expect(memValues(object)).toEqual(['a', 'b']);
});