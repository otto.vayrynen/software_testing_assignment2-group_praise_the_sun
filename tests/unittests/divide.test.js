import divide from '../../src/divide.js';
//divede.js väärä funktion toteutus jakaja jakaa itsellään
test('Kahden positiivisen luvun jakaminen', () => {
    expect(divide(10, 2)).toBe(5);
});

test('Positiivisen luvun jakaminen negatiivisella antaa negatiivisen luvun', () => {
    expect(divide(12, -2)).toBe(-6);
});

test('Negatiivisen luvun jakaminen negatiivisella antaa positiivisen luvun', () => {
    expect(divide(-6, -4)).toBe(1.5);
});

test('Luvun jakaminen nollalla antaa errorin "Division by zero"', () => {
    expect(divide(5, 0)).toThrowError(new Error('Division by zero'));
});

test('Desimaaliluvun jakaminen kokonaisluvulla', () => {
    expect(divide(6.52, 7)).toBe(6.52/7);
});

test('Desimaaliluvun jakaminen desimaaliluvulla', () => {
    expect(divide(0.1, 0.2)).toBe(0.5);
});

test('Irrationaaliluvun jakaminen kokonaisluvulla', () => {
    expect(divide(Math.PI, 2)).toBe(Math.PI/2);
});

test('Irrationaaliluvun jakaminen itsellään antaa arvon 1', () => {
    expect(divide(Math.PI, Math.PI)).toBe(Math.PI/Math.PI);
});

test('Nollan jakaminen', () => {
    expect(divide(0, 4)).toBe(0);
});