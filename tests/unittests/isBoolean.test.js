import isBoolean from '../../src/isBoolean.js';

test('palauta true jos syöte on boolean', () => {
    expect(isBoolean(false)).toBe(true);
    expect(isBoolean(true)).toBe(true);
});

test('palauta false jos syöte ei ole boolean', () => {
    expect(isBoolean(null)).toBe(false);
    expect(isBoolean(Symbol("test"))).toBe(false);
    expect(isBoolean("test")).toBe(false);
    expect(isBoolean(12)).toBe(false);
});