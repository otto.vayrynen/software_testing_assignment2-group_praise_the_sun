import ToNumber from '../../src/ToNumber.js';


test('Luku säilyy lukuna', () => {
    expect(ToNumber(3.2)).toBe(3.2);
});

test('Stringin muuttaminen luvuksi, jos sillä on numeraalinen arvo', () => {
    expect(ToNumber('3.2')).toBe(3.2);
});

test('Jos syöte on symboli, palautetaan NaN', () => {
    expect(ToNumber(Symbol("test"))).toBe(NaN);
});

test('Infinity säilyy Infinitynä', () => {
    expect(ToNumber(Infinity)).toBe(Infinity);
});

test('Number.MIN_VALUE säilyttää arvon 5e-324', () => {
    expect(ToNumber(Number.MIN_VALUE)).toBe(5e-324);
});

test('Objecti palauttaa luvun jos sisältää yhden arvon', () => {
    const reTrim = /^\s+|\s+$/g
    const value = {val:3};
    const other = typeof value.valueOf === 'function' ? value.valueOf() : value
    console.log(value.valueOf)
    console.log(value.valueOf())
    console.log(`${other}`.replace(reTrim, ''))
    expect(ToNumber({value:3})).toBe(3);
});

test('Objecti palauttaa NaN jos sisältää usean arvon', () => {
    const reTrim = /^\s+|\s+$/g
    const value = {val:3, val2:5};
    const other = typeof value.valueOf === 'function' ? value.valueOf() : value
    console.log(value.valueOf)
    console.log(value.valueOf())
    console.log(`${other}`.replace(reTrim, ''))
    expect(ToNumber({val:3, val2:5})).toBe(NaN);
});