import isObject from '../../src/isObject.js';

test('palauta true jos syöte on tyhjä objekti', () => {
    expect(isObject({})).toBe(true);
});

test('palauta true jos syöte on objekti', () => {
    expect(isObject([1, 2, 3, 4])).toBe(true);
});

test('palauta true jos syöte on funktio', () => {
    expect(isObject(Function)).toBe(true);
});

test('palauta false jos syöte ei ole objecti tai funktio', () => {
    expect(isObject(Symbol("test"))).toBe(false);
    expect(isObject(42)).toBe(false);
    expect(isObject("object")).toBe(false);
    expect(isObject(true)).toBe(false);
});

test('palauta false jos syöte on null', () => {
    expect(isObject(null)).toBe(false);

});