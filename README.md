[![Coverage Status](https://coveralls.io/repos/gitlab/otto.vayrynen/software_testing_assignment2-group_praise_the_sun/badge.svg?branch=main)](https://coveralls.io/gitlab/otto.vayrynen/software_testing_assignment2-group_praise_the_sun?branch=main)


Source code folder contains a separate license file that must **NOT** be removed under any circumstances!
Removing this license file directly violates terms and conditions of the software under testing.
Individuals who remove or modify the license file will also carry the consequences.